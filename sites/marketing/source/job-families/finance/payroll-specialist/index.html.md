---
layout: job_family_page
title: "Payroll"
---

## Payroll Specialist

### Job Grade

The Payroll Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Provide prompt, courteous, and efficient customer service to employees who have questions regarding their pay
- Review and approve expense reports
- Assists with Year-End process
- Prepare ad hoc reports as needed
- Assist in the development and documentation of payroll procedures designed to streamline the process and scale with the organization
- Support internal and external payroll audit requests
- Verify and audit data processed by external payroll providers
- Provide timely and accurate response to team members’ payroll related queries

#### Requirements

- Working knowledge of payroll best practices
- Strong knowledge of federal and state regulations
- Flexibility in dealing with multiple payrolls at one time
- Proficient with Google sheets (formula, pivot tablet, etc.)
- Strong work ethic and team player
- Ability to work in a collaborative environment
- High degree of professionalism
- Ability to deal sensitively with confidential material
- Strong verbal and written communication skills
- Decision-making, problem-solving, and analytical skills
- Organizational, multi-tasking, and prioritizing skills
- Demonstrate initiative and handle constantly evolving priorities and contending demands
- Highly organized and attentive to detail when working to tight deadlines
- Ability to collaborate and establish relationships cross-functionally with PeopleOps
- Ability to use GitLab

## Specialities

### US

#### Additional Responsibilities

- Own and process payroll, including tax changes, direct deposits, loan repayments, deduction goals, retroactive adjustments, prorated payments, bonuses, special pays, etc.
- Tax filings - Register new state/local/SUI tax account with local tax agency
- Process pension and benefit changes and funding

#### Additional Requirements

- 3-4  years' experience processing multi-state payroll
- International payroll experience is a plus
- Working knowledge of ADP and Excel

### EMEA

#### Additional Responsibilities

- Prepare and process payroll for non-US team members on a monthly basis for Netherlands, UK, Germany, and Australia
- Review and approve payroll invoices from PEO vendor(s) for payments
- Adhere to payroll procedures and align with relevant country law
- Prepare and process contractor payments through iiPay
- Responsible for timely and accurate submission of all payroll data to required payroll vendors

#### Additional Requirements

- 3-5 years of non-US payroll processing experience
- Previous experience of processing monthly payrolls in Europe

## IC Performance Indicators
- [Payroll accuracy for each check date](/handbook/finance/payroll/#payroll-accuracy-for-each-check-date--100)
- [Payroll journal entry reports submitted to Accounting](/handbook/finance/payroll/#payroll-journal-entry-reports-submitted-to-accounting--payroll-date--2-business-days)

## IC Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their title on our team page.

- Selected candidates will be invited to schedule a screening call with a member of our Recruiting team
- Next, candidates will be invited to schedule a first interview with our Payroll and Payment Lead
- In addition, you may be asked to schedule time with our Senior Accounting and External Reporting Manager
- Next, candidates will be invited to schedule an interview with our Controller
- Candidates will then be invited to schedule an interview with our Senior Total Rewards Analysts
- Next, candidates will be invited to schedule an interview with our Chief Financial Officer
- Finally, candidates may be asked to interview with our CEO.

Additional details about our process can be found on our [hiring page](/handbook/hiring)

## Manager, Payroll and Payments

### Job Grade

The Manager, Payroll and Payments is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Coordinate all global payroll processing operations.
- Ensure that all relevant data is processed and track through the relevant payroll systems.
- Responsible for working with local tax and legal advisors to ensure the company follows local regulations with respect to payroll.
- Work closely with GitLab contributors and the People Ops team to ensure timely response to questions and requests that have been processed.
- Complete filings for payroll subsidies, where applicable.
- Accounting lead for implementation of new payroll systems.
- Accounting lead for other global payroll systems.
- Establish and enforce proper accounting policies and principles.
- Manage Payroll Specialist - US, Payroll Specialist - EMEA, Expense Specialist
- Primary ownership for management and external reporting.
- Development and implementation of new procedures to enhance the workflow of payments to contributors.
- Primary contact point for external audit with respect to payroll matters.
- Coordinate with other auditors as needed.
- Support overall department goals and objectives.

### Requirements

- Proven work experience as a payroll administrator.
- Experience with large payroll platforms such as ADP or Paychex.
- Been responsible for at least one international payroll function.
- Public company experience with Sarbanes Oxley a plus.
- Experience with Netsuite a plus.
- Proficient with google sheets.
- Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
- Ability to use GitLab

## Paryoll Lead

### Job Grade

The Payroll Lead is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Coordinate all global payroll processing operations. Currently the company runs payroll in five countries and has partners in another three countries that provide payroll solutions to contributors.
- Ensure that all relevant data is processed and track through the relevant payroll systems.
- Responsible for working with local tax and legal advisors to ensure the company follows local regulations with respect to payroll.
- Work closely with GitLab contributors and the People Ops team to ensure timely response to questions and requests that have been processed.
- Complete filings for payroll subsidies, where applicable.
- Accounting lead for implementation of new payroll system in the US.
- Accounting lead for other global payroll systems.
- Establish and enforce proper accounting policies and principles.
- Primary ownership for management and external reporting.
- Development and implementation of new procedures to enhance the workflow of payments to contributors.
- Primary contact point for external audit with respect to payroll matters.
- Coordinate with other auditors as needed.
- Support overall department goals and objectives.

### Requirements

- Proven work experience as a payroll administrator.
- Experience with large payroll platforms such as ADP or Paychex.
- Been responsible for at least one international payroll function.
- Public company experience with Sarbanes Oxley a plus.
- Experience with Netsuite a plus.
- Proficient with google sheets.
- Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision

## Senior Manager, Payroll and Payments

Sr Manager, Global Payroll and Payments is responsible for managing payroll and expense reimbursement for roughly 1000 team members in 60 countries. The main focus will be on providing strategic leadership, developing processes, and controls, along with managing the relationship with all payroll providers.

### Job Grade

The Senior Manager, Payroll and Payments is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Essential Duties and Responsibilities

- Manage overall global operations of payrolls and payments
- Leads the strategic direction for the payroll processes while meeting key controls and payroll compliance
- Manage payroll team
- Manage all employee payroll queries and communication, including the Payroll inbox and Slack channels
- Work with PeopleOps team to ensure our HRIS (BambooHR) provides the right information in the best way for payroll integration and reporting
- Develop internal relationships and vendor partnerships to improve process efficiencies
- Develop and prioritize processes to improve the payroll experience for team members
- Ensure all operational processes are controlled from a risk perspective, and have built-in control plans and assessments to regularly review their effectiveness
- Evaluate, develop and enforce the use of audit tools which support the payroll process
- Set up payroll processes in new countries as we expand operations in another country.
- Lead significant systems and process changes in the payroll function.
- Support month end close activities
- Responsible for Worker’s Compensation audits
- Review and analyze current payroll, benefits and tax procedures in order to recommend changes to Company policies resulting in best practices
- Oversees payroll audit process and reporting aspects of internal and external audits; identifies and resolves discrepancies.
- Ensure that all monthly, quarterly and annual payroll tax filings are met; ensure all local statutory payroll and fringe benefit reporting requirements are met.
- Ensure stock activity for US and international employees is properly processed and reported.
- Maintain compliance with all Federal, State, and local payroll tax laws
- Responsible for all system year-end tasks including the reset of 401K, FSA, HSA limits per yearly IRS guidelines
- Prepare and administrator W-2’s for US entities and annual tax documents for non-US entities
- Serve as a subject matter expert and point of contact for escalations and various ad hoc requests
- Manage the expense reimbursement process for all team members
- Special projects as assigned
- Mentor direct reports
- Be a role model for the team, and develop a strong sense of shared team vision
- Maintains professional and technical knowledge by attending educational workshops and reviewing professional publications
- Prepare and manage budget related to payroll programs

## Manager Performance Indicators
- [Payroll accuracy for each check date](/handbook/finance/payroll/#payroll-accuracy-for-each-check-date--100)
- [Payroll journal entry reports submitted to Accounting](/handbook/finance/payroll/#payroll-journal-entry-reports-submitted-to-accounting--payroll-date--2-business-days)
- [Percentage of ineffective SOX Controls](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Manager Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team.
* Next, candidates will be invited to schedule a first interview with our Controller.
* Next, candidates will be invited to schedule an interview with a People Business Partner.
* Candidates will then be invited to schedule an interview with our CFO.
* Finally, candidates may be asked to interview with our CEO.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Career Ladder

The next step in the Payroll job family is to move a higher level leadership payroll role which is not yet defined at GitLab. 
